package ru.aushakov.tm.repository;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.repository.IBusinessRepository;
import ru.aushakov.tm.enumerated.Status;
import ru.aushakov.tm.exception.general.InvalidIndexException;
import ru.aushakov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity>
        extends AbstractRepository<E> implements IBusinessRepository<E> {

    @Override
    @NotNull
    public List<E> findAll(@NotNull final String userId) {
        return list.stream().filter(e -> userId.equals(e.getUserId())).collect(Collectors.toList());
    }

    @Override
    @NotNull
    public List<E> findAll(
            @NotNull final String userId,
            @NotNull final Comparator<E> comparator
    ) {
        return list.stream().filter(e -> userId.equals(e.getUserId()))
                .sorted(comparator).collect(Collectors.toList());
    }

    @Override
    public void clear(@NotNull final String userId) {
        list.removeAll(list.stream().filter(e -> userId.equals(e.getUserId())).collect(Collectors.toList()));
    }

    @Override
    @Nullable
    public E findOneById(@NotNull final String id, @NotNull final String userId) {
        return list.stream()
                .filter(e -> (id.equals(e.getId()) && userId.equals(e.getUserId())))
                .findFirst().orElse(null);
    }

    @Override
    @Nullable
    public E findOneByIndex(@NotNull final Integer index, @NotNull final String userId) {
        @NotNull final E entity = Optional.ofNullable(list.get(index))
                .orElseThrow(InvalidIndexException::new);
        if (userId.equals(entity.getUserId())) return entity;
        return null;
    }

    @Override
    @Nullable
    public E findOneByName(@NotNull final String name, @NotNull final String userId) {
        return list.stream()
                .filter(e -> (name.equals(e.getName()) && userId.equals(e.getUserId())))
                .findFirst().orElse(null);
    }

    @Override
    @Nullable
    public E removeOneById(@NotNull final String id, @NotNull final String userId) {
        @Nullable final E entity = findOneById(id, userId);
        Optional.ofNullable(entity).ifPresent(list::remove);
        return entity;
    }

    @Override
    @Nullable
    public E removeOneByIndex(@NotNull final Integer index, @NotNull final String userId) {
        @Nullable final E entity = findOneByIndex(index, userId);
        Optional.ofNullable(entity).ifPresent(list::remove);
        return entity;
    }

    @Override
    @Nullable
    public E removeOneByName(@NotNull final String name, @NotNull final String userId) {
        @Nullable final E entity = findOneByName(name, userId);
        Optional.ofNullable(entity).ifPresent(list::remove);
        return entity;
    }

    @Override
    @Nullable
    public E updateOneById(
            @NotNull final String id,
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @Nullable final E entity = findOneById(id, userId);
        Optional.ofNullable(entity).ifPresent(e -> {
            e.setName(name);
            e.setDescription(description);
        });
        return entity;
    }

    @Override
    @Nullable
    public E updateOneByIndex(
            @NotNull final Integer index,
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @Nullable final E entity = findOneByIndex(index, userId);
        Optional.ofNullable(entity).ifPresent(e -> {
            e.setName(name);
            e.setDescription(description);
        });
        return entity;
    }

    @Override
    @Nullable
    public E startOneById(@NotNull final String id, @NotNull final String userId) {
        @Nullable final E entity = findOneById(id, userId);
        Optional.ofNullable(entity).ifPresent(e -> e.setStatus(Status.IN_PROGRESS));
        return entity;
    }

    @Override
    @Nullable
    public E startOneByIndex(@NotNull final Integer index, @NotNull final String userId) {
        @Nullable final E entity = findOneByIndex(index, userId);
        Optional.ofNullable(entity).ifPresent(e -> e.setStatus(Status.IN_PROGRESS));
        return entity;
    }

    @Override
    @Nullable
    public E startOneByName(@NotNull final String name, @NotNull final String userId) {
        @Nullable final E entity = findOneByName(name, userId);
        Optional.ofNullable(entity).ifPresent(e -> e.setStatus(Status.IN_PROGRESS));
        return entity;
    }

    @Override
    @Nullable
    public E finishOneById(@NotNull final String id, @NotNull final String userId) {
        @Nullable final E entity = findOneById(id, userId);
        Optional.ofNullable(entity).ifPresent(e -> e.setStatus(Status.COMPLETED));
        return entity;
    }

    @Override
    @Nullable
    public E finishOneByIndex(@NotNull final Integer index, @NotNull final String userId) {
        @Nullable final E entity = findOneByIndex(index, userId);
        Optional.ofNullable(entity).ifPresent(e -> e.setStatus(Status.COMPLETED));
        return entity;
    }

    @Override
    @Nullable
    public E finishOneByName(@NotNull final String name, @NotNull final String userId) {
        @Nullable final E entity = findOneByName(name, userId);
        Optional.ofNullable(entity).ifPresent(e -> e.setStatus(Status.COMPLETED));
        return entity;
    }

    @Override
    @Nullable
    public E changeOneStatusById(
            @NotNull final String id,
            @NotNull final Status status,
            @NotNull final String userId
    ) {
        @Nullable final E entity = findOneById(id, userId);
        Optional.ofNullable(entity).ifPresent(e -> e.setStatus(status));
        return entity;
    }

    @Override
    @Nullable
    public E changeOneStatusByIndex(
            @NotNull final Integer index,
            @NotNull final Status status,
            @NotNull final String userId
    ) {
        @Nullable final E entity = findOneByIndex(index, userId);
        Optional.ofNullable(entity).ifPresent(e -> e.setStatus(status));
        return entity;
    }

    @Override
    @Nullable
    public E changeOneStatusByName(
            @NotNull final String name,
            @NotNull final Status status,
            @NotNull final String userId
    ) {
        @Nullable final E entity = findOneByName(name, userId);
        Optional.ofNullable(entity).ifPresent(e -> e.setStatus(status));
        return entity;
    }

}
