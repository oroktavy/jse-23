package ru.aushakov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;

public class EmptyIdException extends RuntimeException {

    public EmptyIdException() {
        super("Provided id is empty!");
    }

    public EmptyIdException(@NotNull final String entityName) {
        super("Provided " + entityName + " id is empty!");
    }

}
