package ru.aushakov.tm.api.repository;

import ru.aushakov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    Task assignTaskToProject(String taskId, String projectId, String userId);

    Task unbindTaskFromProject(String taskId, String userId);

    List<Task> findAllTasksByProjectId(String projectId, String userId);

    List<Task> removeAllTasksByProjectId(String projectId, String userId);

}
