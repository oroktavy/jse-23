package ru.aushakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.constant.ArgumentConst;
import ru.aushakov.tm.constant.TerminalConst;

public final class AboutCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.CMD_ABOUT;
    }

    @Override
    @NotNull
    public String getArgument() {
        return ArgumentConst.ARG_ABOUT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show general application info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: ANDREY USHAKOV");
        System.out.println("E-MAIL: oroktavy@gmail.com");
    }

}
