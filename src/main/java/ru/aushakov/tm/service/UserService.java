package ru.aushakov.tm.service;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.repository.IUserRepository;
import ru.aushakov.tm.api.service.IUserService;
import ru.aushakov.tm.enumerated.Role;
import ru.aushakov.tm.exception.empty.EmptyIdException;
import ru.aushakov.tm.exception.empty.EmptyLoginException;
import ru.aushakov.tm.exception.empty.EmptyPasswordException;
import ru.aushakov.tm.exception.general.InvalidRoleException;
import ru.aushakov.tm.model.User;
import ru.aushakov.tm.util.HashUtil;

public final class UserService extends AbstractService<User> implements IUserService {

    @NonNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        this.repository = userRepository;
        this.userRepository = userRepository;
    }

    @Override
    @Nullable
    public User findOneByLogin(@Nullable final String login) {
        if (StringUtils.isEmpty(login)) throw new EmptyLoginException();
        return userRepository.findOneByLogin(login);
    }

    @Override
    @Nullable
    public User removeOneByLogin(@Nullable final String login) {
        if (StringUtils.isEmpty(login)) throw new EmptyLoginException();
        return userRepository.removeOneByLogin(login);
    }

    @Override
    @NotNull
    public User add(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (StringUtils.isEmpty(login)) throw new EmptyLoginException();
        if (StringUtils.isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final User user = new User(login, HashUtil.salt(password));
        if (!StringUtils.isEmpty(email)) user.setEmail(email);
        userRepository.add(user);
        return user;
    }

    @Override
    public void add(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final String roleId
    ) {
        if (StringUtils.isEmpty(login)) throw new EmptyLoginException();
        if (StringUtils.isEmpty(password)) throw new EmptyPasswordException();
        @Nullable final Role role = Role.toRole(roleId);
        if (role == null) throw new InvalidRoleException(roleId);
        @NotNull final User user = new User(login, HashUtil.salt(password));
        if (!StringUtils.isEmpty(email)) user.setEmail(email);
        user.setRole(role);
        userRepository.add(user);
    }

    @Override
    @Nullable
    public User setPassword(
            @Nullable final String login,
            @Nullable final String newPassword
    ) {
        if (StringUtils.isEmpty(login)) throw new EmptyLoginException();
        if (StringUtils.isEmpty(newPassword)) throw new EmptyPasswordException();
        return userRepository.setPassword(login, newPassword);
    }

    @Override
    @Nullable
    public User updateOneById(
            @Nullable final String id,
            @Nullable final String lastName,
            @Nullable final String firstName,
            @Nullable final String middleName
    ) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return userRepository.updateOneById(id, lastName, firstName, middleName);
    }

    @Override
    @Nullable
    public User updateOneByLogin(
            @Nullable final String login,
            @Nullable final String lastName,
            @Nullable final String firstName,
            @Nullable final String middleName
    ) {
        if (StringUtils.isEmpty(login)) throw new EmptyLoginException();
        return userRepository.updateOneByLogin(login, lastName, firstName, middleName);
    }

    @Override
    @Nullable
    public User lockOneByLogin(@Nullable final String login) {
        if (StringUtils.isEmpty(login)) throw new EmptyLoginException();
        return userRepository.lockOneByLogin(login);
    }

    @Override
    @Nullable
    public User unlockOneByLogin(@Nullable final String login) {
        if (StringUtils.isEmpty(login)) throw new EmptyLoginException();
        return userRepository.unlockOneByLogin(login);
    }

}
