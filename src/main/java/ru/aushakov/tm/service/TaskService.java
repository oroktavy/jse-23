package ru.aushakov.tm.service;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.repository.ITaskRepository;
import ru.aushakov.tm.api.service.ITaskService;
import ru.aushakov.tm.exception.empty.EmptyIdException;
import ru.aushakov.tm.exception.general.NoUserLoggedInException;
import ru.aushakov.tm.model.Task;

import java.util.List;

public final class TaskService extends AbstractBusinessService<Task> implements ITaskService {

    @NonNull
    private final ITaskRepository taskRepository;

    public TaskService(@NotNull ITaskRepository taskRepository, @NotNull Class currentClass) {
        this.taskRepository = taskRepository;
        this.businessRepository = taskRepository;
        this.repository = taskRepository;
        this.currentClass = currentClass;
    }

    @Override
    @Nullable
    public Task assignTaskToProject(
            @Nullable final String taskId,
            @Nullable final String projectId,
            @Nullable final String userId
    ) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(taskId)) throw new EmptyIdException("task");
        if (StringUtils.isEmpty(projectId)) throw new EmptyIdException("project");
        return taskRepository.assignTaskToProject(taskId, projectId, userId);
    }

    @Override
    @Nullable
    public Task unbindTaskFromProject(@Nullable final String taskId, @Nullable final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(taskId)) throw new EmptyIdException("task");
        return taskRepository.unbindTaskFromProject(taskId, userId);
    }

    @Override
    @NotNull
    public List<Task> findAllTasksByProjectId(@Nullable final String projectId, @Nullable final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(projectId)) throw new EmptyIdException("project");
        return taskRepository.findAllTasksByProjectId(projectId, userId);
    }

}
