package ru.aushakov.tm.service;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.repository.IUserRepository;
import ru.aushakov.tm.api.service.IAuthService;
import ru.aushakov.tm.enumerated.Role;
import ru.aushakov.tm.exception.empty.EmptyLoginException;
import ru.aushakov.tm.exception.empty.EmptyPasswordException;
import ru.aushakov.tm.exception.empty.EmptyRoleException;
import ru.aushakov.tm.exception.entity.UserNotFoundException;
import ru.aushakov.tm.exception.general.NoPermissionToExecuteException;
import ru.aushakov.tm.exception.general.NoUserLoggedInException;
import ru.aushakov.tm.exception.general.WrongCredentialsException;
import ru.aushakov.tm.model.User;
import ru.aushakov.tm.util.HashUtil;

import java.util.Arrays;
import java.util.Optional;

public final class AuthService implements IAuthService {

    @Nullable
    private User authenticatedUser;

    @NonNull
    private final IUserRepository userRepository;

    public AuthService(@NotNull IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (StringUtils.isEmpty(login)) throw new EmptyLoginException();
        if (StringUtils.isEmpty(password)) throw new EmptyPasswordException();
        @Nullable final User user = userRepository.findOneByLogin(login);
        if (user == null || !HashUtil.salt(password).equals(user.getPasswordHash()) || user.isLockedFlag()) {
            throw new WrongCredentialsException();
        }
        authenticatedUser = user;
    }

    @Override
    public void logout() {
        authenticatedUser = null;
    }

    @Override
    public boolean isUserAuthenticated() {
        return (authenticatedUser != null);
    }

    @Override
    @NotNull
    public String getUserId() {
        Optional.ofNullable(authenticatedUser).orElseThrow(NoUserLoggedInException::new);
        return authenticatedUser.getId();
    }

    @Override
    @NotNull
    public String getUserLogin() {
        Optional.ofNullable(authenticatedUser).orElseThrow(NoUserLoggedInException::new);
        return authenticatedUser.getLogin();
    }

    @Override
    @NotNull
    public User getUser() {
        Optional.ofNullable(authenticatedUser).orElseThrow(NoUserLoggedInException::new);
        @NotNull final String login = authenticatedUser.getLogin();
        authenticatedUser = Optional.ofNullable(userRepository.findOneByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        return authenticatedUser;
    }

    @Override
    public void changePassword(@Nullable final String oldPassword, @Nullable final String newPassword) {
        Optional.ofNullable(authenticatedUser).orElseThrow(NoUserLoggedInException::new);
        if (StringUtils.isEmpty(oldPassword) || StringUtils.isEmpty(newPassword)) {
            throw new EmptyPasswordException();
        }
        @NotNull final String login = authenticatedUser.getLogin();
        @NotNull final String oldPasswordHash = authenticatedUser.getPasswordHash();
        if (!oldPasswordHash.equals(HashUtil.salt(oldPassword))) throw new WrongCredentialsException();
        userRepository.setPassword(login, newPassword);
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null || roles.length < 1) return;
        @NotNull final Role role = Optional.ofNullable(authenticatedUser)
                .orElseThrow(NoUserLoggedInException::new).getRole();
        Arrays.stream(roles)
                .filter(r -> role.name().equals(Optional.ofNullable(r).orElseThrow(EmptyRoleException::new).name()))
                .findFirst().orElseThrow(NoPermissionToExecuteException::new);
    }

}
