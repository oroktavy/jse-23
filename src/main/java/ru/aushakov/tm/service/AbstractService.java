package ru.aushakov.tm.service;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.repository.IRepository;
import ru.aushakov.tm.api.service.IService;
import ru.aushakov.tm.exception.empty.EmptyIdException;
import ru.aushakov.tm.exception.entity.NoEntityProvidedException;
import ru.aushakov.tm.exception.general.InvalidIndexException;
import ru.aushakov.tm.exception.general.NoComparatorProvidedException;
import ru.aushakov.tm.model.AbstractEntity;
import ru.aushakov.tm.util.NumberUtil;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NonNull
    protected IRepository<E> repository;

    protected AbstractService(@NotNull IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    @NotNull
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    @NotNull
    public List<E> findAll(@Nullable final Comparator<E> comparator) {
        return repository.findAll(Optional.ofNullable(comparator).orElseThrow(NoComparatorProvidedException::new));
    }

    @Override
    public void add(@Nullable final E entity) {
        repository.add(Optional.ofNullable(entity).orElseThrow(NoEntityProvidedException::new));
    }

    @Override
    public void remove(@Nullable final E entity) {
        repository.remove(Optional.ofNullable(entity).orElseThrow(NoEntityProvidedException::new));
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    @Nullable
    public E findOneById(@Nullable final String id) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return repository.findOneById(id);
    }

    @Override
    @Nullable
    public E findOneByIndex(@NotNull final Integer index) {
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return repository.findOneByIndex(index);
    }

    @Override
    @Nullable
    public E removeOneById(@Nullable final String id) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return repository.removeOneById(id);
    }

    @Override
    @Nullable
    public E removeOneByIndex(@NotNull final Integer index) {
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return repository.removeOneByIndex(index);
    }

}
