package ru.aushakov.tm.service;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.repository.ICommandRepository;
import ru.aushakov.tm.api.service.ICommandService;
import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.exception.empty.EmptyArgumentException;
import ru.aushakov.tm.exception.empty.EmptyCommandException;
import ru.aushakov.tm.exception.empty.EmptyNameException;

import java.util.Collection;

public final class CommandService implements ICommandService {

    @NonNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        if (StringUtils.isEmpty(command.getName())) throw new EmptyNameException(command);
        commandRepository.add(command);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@Nullable final String command) {
        if (StringUtils.isEmpty(command)) throw new EmptyCommandException();
        return commandRepository.getCommandByName(command);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArg(@Nullable final String arg) {
        if (StringUtils.isEmpty(arg)) throw new EmptyArgumentException();
        return commandRepository.getCommandByArg(arg);
    }

}
